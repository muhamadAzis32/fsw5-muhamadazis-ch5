const mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    rentPerday: {
        type: String,
        required: true,
    },
    size: String,
    foto: String,
    date: {
        type: Date,
        // `Date.now()` returns the current unix timestamp as a number
        default: Date.now
    },
})

const Cardb = mongoose.model('tbMobil', schema);

module.exports = Cardb;