# Car Management Dashboard

Membuat HTTP Server yang dapat digunakan untuk melakukan manajemen data mobil, manajemen data mobil meliputi aksi-aksi berikut Menambahkan data mobil, Memodiﬁkasi data mobil yang sudah ada, Menghapus data mobil yang sudah ada, Melihat daftar mobil yang tersedia di dalam database, untuk database nya sendiri menggunakan nosql yaitu mongoDB.

Untuk menjalankan HTTP server tersebut, cukup jalankan perintah berikut di dalam terminal :

Install dependency
```bash
npm install express axios body-parser dotenv ejs express-flash-messages express-session moment mongoose morgan multer nodemon
```

Konfigurasi dengan membuat file config.env
```bash
PORT=3000
MONGO_URI=mongodb://localhost:27017/dbMobil
```

Jalankan server
```bash
npm start
```

## Entity Relationship Diagram [ERD](https://dbdiagram.io/d/625e65bd2514c9790353450e)
![Untitled](https://user-images.githubusercontent.com/88225954/164626368-4f90e5c7-f0db-4da1-9135-da79a2f46ae6.png)


## Contoh Tampilan
![ch5-1](https://user-images.githubusercontent.com/88225954/164625992-034417c6-8f51-4db8-9501-a44cd23280d3.png)
![ch5-2](https://user-images.githubusercontent.com/88225954/164626288-913d0c62-0df8-4d93-9755-0eb84cce72f8.png)


## list Car

Untuk membuka list Car, gunakan browser dan bukalah URL berikut:

- [list Car](http://localhost:3000)


